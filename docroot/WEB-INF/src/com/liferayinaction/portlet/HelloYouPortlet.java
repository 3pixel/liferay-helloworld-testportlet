package com.liferayinaction.portlet;

import java.io.IOException;

import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.PortletPreferences;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class HelloYouPortlet extends GenericPortlet {
	
	protected String editJSP;
	protected String viewJSP;
	private static Log _log = LogFactory.getLog(HelloYouPortlet.class);
	
	public void init() throws PortletException{
		editJSP = getInitParameter("edt-jsp");
		viewJSP = getInitParameter("view-jsp");
	}
	
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
		throws IOException, PortletException{
		
		PortletPreferences prefs = renderRequest.getPreferences();
		String userName = (String) prefs.getValue("name", "no");
		if(userName.equalsIgnoreCase("no")){
			userName = "";
		}
		
		renderRequest.setAttribute("username", userName);
		include(viewJSP, renderRequest, renderResponse);
	}

	protected void include(String path, RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		
		
		PortletRequestDispatcher portletRequestDispatcher = getPortletContext().getRequestDispatcher(path);
		
		if(portletRequestDispatcher == null){
			_log.error(path + " is not a valid include");
		}else{
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
		
	}
}
